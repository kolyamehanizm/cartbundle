<?php
namespace becompact\CartBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class CartBundle extends Bundle
{
    public function getPath(): string
    {
        return dirname(__DIR__);
    }
}
