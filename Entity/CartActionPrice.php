<?php


namespace becompact\CartBundle\Entity;

use becompact\CartBundle\Repository\CartActionPriceRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * Действие для цены
 * @ORM\Entity(repositoryClass=CartActionPriceRepository::class)
 * @ORM\Table(name="cart_action_prices", indexes={
 *     @ORM\Index(name="cart_action_prices_id_idx", columns={"id"}),
 *     @ORM\Index(name="cart_action_prices_tree_idx", columns={"tree_id"}),
 * })
 */
class CartActionPrice
{
    /**
     * Идентификатор
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Наименование
     * @ORM\Column(type="string", name="name", length=255, nullable=true)
     */
    private $name;

    /**
     * Идентификатор дерева
     * @ORM\Column(type="integer", name="tree_id", nullable=true)
     */
    private $treeId;

    /**
     * Бесплатно
     * @ORM\Column(type="boolean", name="free", nullable=true)
     */
    private $free;

    /**
     * Скидка
     * @ORM\Column(type="float", name="discount", nullable=true)
     */
    private $discount;

    /**
     * Скидка в процентах
     * @ORM\Column(type="boolean", name="percent", nullable=true)
     */
    private $percent;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return $this
     */
    public function setName(?string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getTreeId(): ?int
    {
        return $this->treeId;
    }

    /**
     * @param int|null $treeId
     * @return $this
     */
    public function setTreeId(?int $treeId): self
    {
        $this->treeId = $treeId;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getFree(): ?bool
    {
        return $this->free;
    }

    /**
     * @param bool|null $free
     * @return $this
     */
    public function setFree(?bool $free): self
    {
        $this->free = $free;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getPercent(): ?bool
    {
        return $this->percent;
    }

    /**
     * @param bool|null $percent
     * @return $this
     */
    public function setPercent(?bool $percent): self
    {
        $this->percent = $percent;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getDiscount(): ?float
    {
        return $this->discount;
    }

    /**
     * @param float|null $discount
     * @return $this
     */
    public function setDiscount(?float $discount): self
    {
        $this->discount = $discount;
        return $this;
    }

}
