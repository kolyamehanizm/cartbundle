<?php

namespace becompact\CartBundle\Entity;

use becompact\CartBundle\Repository\CartConditionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CartConditionRepository::class)
 * @ORM\Table(name="cart_conditions", indexes={
 *     @ORM\Index(name="cart_conditions_id_idx", columns={"id"}),
 *     @ORM\Index(name="cart_conditions_active_idx", columns={"active"}),
 *     @ORM\Index(name="cart_conditions_tree_idx", columns={"tree_id"}),
 *     @ORM\Index(name="cart_conditions_active_id_idx", columns={"active","id"}),
 *     @ORM\Index(name="cart_conditions_active_tree_idx", columns={"active","tree_id"}),
 * })
 */
class CartCondition
{
    /**
     * Идентификатор
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Наименование
     *
     * @ORM\Column(type="string", name="name", length=255, nullable=true)
     */
    private $name;

    /**
     * Идентификатор дерева
     *
     * @ORM\Column(type="integer", name="tree_id", nullable=true)
     */
    private $treeId;

    /**
     * Активность
     *
     * @ORM\Column(type="boolean", name="active", nullable=true)
     */
    private $active;

    /**
     * Артикулы товара для правила
     *
     * @ORM\OneToOne(targetEntity=ConditionArticles::class, mappedBy="condition", cascade={"persist", "remove"})
     */
    private $articles;

    /**
     * Свойства товара для правила
     *
     * @ORM\OneToMany(targetEntity=ConditionProperties::class, mappedBy="condition")
     */
    private $properties;

    /**
     * Бренды для правила
     *
     * @ORM\OneToOne(targetEntity=ConditionBrands::class, mappedBy="condition", cascade={"persist", "remove"})
     */
    private $brands;

    /**
     *
     *
     * @ORM\OneToOne(targetEntity=ConditionPrices::class, mappedBy="condition", cascade={"persist", "remove"})
     */
    private $prices;

    /**
     * @ORM\OneToOne(targetEntity=ConditionCounts::class, mappedBy="condition", cascade={"persist", "remove"})
     */
    private $counts;

    /**
     * Доставки для корзины
     *
     * @ORM\OneToOne(targetEntity=ConditionDeliveries::class, mappedBy="condition", cascade={"persist", "remove"})
     */
    private $deliveries;

    /**
     * Категории для корзины
     *
     * @ORM\OneToOne(targetEntity=ConditionCategories::class, mappedBy="condition", cascade={"persist", "remove"})
     */
    private $categories;


    public function __construct()
    {
        $this->properties = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTreeId(): ?int
    {
        return $this->treeId;
    }

    public function setTreeId(?int $treeId): self
    {
        $this->treeId = $treeId;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getArticles(): ?ConditionArticles
    {
        return $this->articles;
    }

    public function setArticles(?ConditionArticles $articles): self
    {
        $this->articles = $articles;

        return $this;
    }

    /**
     * @return Collection|ConditionProperties[]
     */
    public function getProperties(): Collection
    {
        return $this->properties;
    }

    public function addProperty(ConditionProperties $property): self
    {
        if (!$this->properties->contains($property)) {
            $this->properties[] = $property;
            $property->setCondition($this);
        }
        return $this;
    }

    public function removeProperty(ConditionProperties $property): self
    {
        if ($this->properties->removeElement($property)) {
            // set the owning side to null (unless already changed)
            if ($property->getCondition() === $this) {
                $property->setCondition(null);
            }
        }
        return $this;
    }

    public function getBrands(): ?ConditionBrands
    {
        return $this->brands;
    }

    public function setBrands(?ConditionBrands $brands): self
    {
        $this->brands = $brands;
        return $this;
    }

    public function getPrices(): ?ConditionPrices
    {
        return $this->prices;
    }

    public function setPrices(?ConditionPrices $prices): self
    {
        $this->prices = $prices;
        return $this;
    }

    public function getCounts(): ?ConditionCounts
    {
        return $this->counts;
    }

    public function setCounts(?ConditionCounts $counts): self
    {
        $this->counts = $counts;
        return $this;
    }

    public function getDeliveries(): ?ConditionDeliveries
    {
        return $this->deliveries;
    }

    public function setDeliveries(?ConditionDeliveries $deliveries): self
    {
        $this->deliveries = $deliveries;
        return $this;
    }

    public function getCategories(): ?ConditionCategories
    {
        return $this->categories;
    }

    public function setCategories(?ConditionCategories $categories): self
    {
        $this->categories = $categories;
        return $this;
    }

}
