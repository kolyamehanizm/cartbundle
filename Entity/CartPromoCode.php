<?php


namespace becompact\CartBundle\Entity;

use becompact\CartBundle\Repository\CartPromoCodeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CartPromoCodeRepository::class)
 * @ORM\Table(name="cart_promo_codes", indexes={
 *     @ORM\Index(name="cart_promo_code_id_idx", columns={"id"}),
 *     @ORM\Index(name="cart_promo_code_active_idx", columns={"active"}),
 *     @ORM\Index(name="cart_promo_code_tree_idx", columns={"tree_id"}),
 *     @ORM\Index(name="cart_promo_code_active_id_idx", columns={"active","id"}),
 *     @ORM\Index(name="cart_promo_code_active_tree_idx", columns={"active","tree_id"}),
 * })
 */
class CartPromoCode
{
    /**
     * Идентификатор
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Наименование
     * @ORM\Column(type="string", name="name", length=255, nullable=true)
     */
    private $name;

    /**
     * Код
     * @ORM\Column(type="string", name="code", length=64, nullable=true)
     */
    private $code;

    /**
     * Идентификатор дерева
     * @ORM\Column(type="integer", name="tree_id", nullable=true)
     */
    private $treeId;

    /**
     * Активность
     * @ORM\Column(type="boolean", name="active", nullable=true)
     */
    private $active;


    /**
     * Описание промокода
     * @ORM\Column(type="string", name="description", length=255, nullable=true)
     */
    private $description;


    /**
     * Идентификатор правила корзины
     * @ORM\Column(type="integer", name="condition_id", nullable=true)
     */
    private $conditionId;

    /**
     * Идентификатор действия для цены товара
     * @ORM\Column(type="integer", name="action_price_id", nullable=true)
     */
    private $actionPriceId;

    /**
     * Промокод не действует если применены ещё промокоды
     * @ORM\Column(type="boolean", name="alone", nullable=true)
     */
    private $alone;

    /**
     * Промокод можно применять много раз
     * @ORM\Column(type="boolean", name="reusable", nullable=true)
     */
    private $reusable;

    /**
     * Сколько раз использовался промокод
     * @ORM\Column(type="integer", name="applied", nullable=true, options={"default" : 0})
     */
    private $applied = 0;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;
        return $this;
    }

    public function getTreeId(): ?int
    {
        return $this->treeId;
    }

    public function setTreeId(?int $treeId): self
    {
        $this->treeId = $treeId;
        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;
        return $this;
    }

    public function getConditionId(): ?int
    {
        return $this->conditionId;
    }

    public function setConditionId(?int $id): self
    {
        $this->conditionId = $id;
        return $this;
    }

    public function getActionPriceId(): ?int
    {
        return $this->actionPriceId;
    }

    public function setActionPriceId(?int $id): self
    {
        $this->actionPriceId = $id;
        return $this;
    }

    public function getAlone(): ?bool
    {
        return $this->alone;
    }

    public function setAlone(?bool $alone): self
    {
        $this->alone = $alone;
        return $this;
    }

    public function getReusable(): ?bool
    {
        return $this->reusable;
    }

    public function setReusable(?bool $reusable): self
    {
        $this->reusable = $reusable;
        return $this;
    }

    public function getApplied(): ?int
    {
        return $this->applied;
    }

    public function setApplied(?int $cnt): self
    {
        $this->applied = $cnt;
        return $this;
    }
}
