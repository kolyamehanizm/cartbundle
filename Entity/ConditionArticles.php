<?php

namespace becompact\CartBundle\Entity;

use becompact\CartBundle\Repository\ConditionArticlesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ConditionArticlesRepository::class)
 * @ORM\Table(name="cart_condition_articles", indexes={
 *     @ORM\Index(name="cart_condition_article_id_idx", columns={"id"}),
 *     @ORM\Index(name="cart_condition_article_rule_idx", columns={"condition_id"}),
 * })
 */
class ConditionArticles
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", name="condition_id", nullable=true)
     */
    private $conditionId;

    /**
     * @ORM\Column(type="json", name="include", nullable=true)
     */
    private $include = [];

    /**
     * @ORM\Column(type="json", name="exclude", nullable=true)
     */
    private $exclude = [];

    /**
     * @ORM\OneToOne(targetEntity=CartCondition::class, inversedBy="articles")
     * @ORM\JoinColumn(name="condition_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $condition;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getConditionId(): ?int
    {
        return $this->conditionId;
    }

    public function setConditionId(?int $id): self
    {
        $this->conditionId = $id;
        return $this;
    }

    public function getInclude(): ?array
    {
        return $this->include;
    }

    public function setInclude(?array $include): self
    {
        $this->include = $include;

        return $this;
    }

    public function getExclude(): ?array
    {
        return $this->exclude;
    }

    public function setExclude(?array $exclude): self
    {
        $this->exclude = $exclude;

        return $this;
    }

    public function getCondition(): ?CartCondition
    {
        return $this->condition;
    }

    public function setCondition(?CartCondition $condition): self
    {
        // unset the owning side of the relation if necessary
        if ($condition === null && $this->condition !== null) {
            $this->condition->setArticles(null);
        }

        // set the owning side of the relation if necessary
        if ($condition !== null && $condition->getArticles() !== $this) {
            $condition->setArticles($this);
        }

        $this->condition = $condition;

        return $this;
    }
}
