<?php

namespace becompact\CartBundle\Entity;

use becompact\CartBundle\Repository\ConditionCountsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ConditionCountsRepository::class)
 * @ORM\Table(name="cart_condition_counts", indexes={
 *     @ORM\Index(name="cart_condition_counts_id_idx", columns={"id"}),
 *     @ORM\Index(name="cart_condition_counts_rule_idx", columns={"condition_id"}),
 * })
 */
class ConditionCounts
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", name="condition_id", nullable=true)
     */
    private $conditionId;

    /**
     * @ORM\Column(type="integer", name="min_value", nullable=true)
     */
    private $min;

    /**
     * @ORM\Column(type="boolean", name="multiple", nullable=true)
     */
    private $multiple;

    /**
     * @ORM\OneToOne(targetEntity=CartCondition::class, inversedBy="counts")
     * @ORM\JoinColumn(name="condition_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $condition;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getConditionId(): ?int
    {
        return $this->conditionId;
    }

    public function setConditionId(?int $id): self
    {
        $this->conditionId = $id;
        return $this;
    }

    public function getMin(): ?int
    {
        return $this->min;
    }

    public function setMin(?int $min): self
    {
        $this->min = $min;

        return $this;
    }

    public function getMultiple(): ?bool
    {
        return $this->multiple;
    }

    public function setMultiple(?bool $multiple): self
    {
        $this->multiple = $multiple;

        return $this;
    }

    public function getCondition(): ?CartCondition
    {
        return $this->condition;
    }

    public function setCondition(?CartCondition $condition): self
    {
        // unset the owning side of the relation if necessary
        if ($condition === null && $this->condition !== null) {
            $this->condition->setCounts(null);
        }

        // set the owning side of the relation if necessary
        if ($condition !== null && $condition->getCounts() !== $this) {
            $condition->setCounts($this);
        }

        $this->condition = $condition;

        return $this;
    }
}
