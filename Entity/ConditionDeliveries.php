<?php

namespace becompact\CartBundle\Entity;

use becompact\CartBundle\Repository\ConditionDeliveriesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ConditionDeliveriesRepository::class)
 * @ORM\Table(name="cart_condition_deliveries", indexes={
 *     @ORM\Index(name="cart_condition_deliveries_id_idx", columns={"id"}),
 *     @ORM\Index(name="cart_condition_deliveries_rule_idx", columns={"condition_id"}),
 * })
 */
class ConditionDeliveries
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", name="condition_id", nullable=true)
     */
    private $conditionId;

    /**
     * @ORM\Column(type="json", name="deliveries", nullable=true)
     */
    private $deliveries = [];

    /**
     * @ORM\OneToOne(targetEntity=CartCondition::class, inversedBy="deliveries")
     * @ORM\JoinColumn(name="condition_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $condition;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getConditionId(): ?int
    {
        return $this->conditionId;
    }

    public function setConditionId(?int $id): self
    {
        $this->conditionId = $id;
        return $this;
    }

    public function getDeliveries(): ?array
    {
        return $this->deliveries;
    }

    public function setDeliveries(?array $deliveries): self
    {
        $this->deliveries = $deliveries;

        return $this;
    }

    public function getCondition(): ?CartCondition
    {
        return $this->condition;
    }

    public function setCondition(?CartCondition $condition): self
    {
        // unset the owning side of the relation if necessary
        if ($condition === null && $this->condition !== null) {
            $this->condition->setDeliveries(null);
        }

        // set the owning side of the relation if necessary
        if ($condition !== null && $condition->getDeliveries() !== $this) {
            $condition->setDeliveries($this);
        }

        $this->condition = $condition;

        return $this;
    }
}
