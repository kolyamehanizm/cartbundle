<?php

namespace becompact\CartBundle\Entity;

use becompact\CartBundle\Repository\ConditionPricesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ConditionPricesRepository::class)
 * @ORM\Table(name="cart_condition_prices", indexes={
 *     @ORM\Index(name="cart_condition_prices_id_idx", columns={"id"}),
 *     @ORM\Index(name="cart_condition_prices_rule_idx", columns={"condition_id"}),
 * })
 */
class ConditionPrices
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", name="condition_id", nullable=true)
     */
    private $conditionId;

    /**
     * @ORM\Column(type="float", name="min_value", nullable=true)
     */
    private $min;

    /**
     * @ORM\Column(type="float", name="max_value", nullable=true)
     */
    private $max;

    /**
     * @ORM\OneToOne(targetEntity=CartCondition::class, inversedBy="prices")
     * @ORM\JoinColumn(name="condition_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $condition;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getConditionId(): ?int
    {
        return $this->conditionId;
    }

    public function setConditionId(?int $id): self
    {
        $this->conditionId = $id;
        return $this;
    }

    public function getMin(): ?float
    {
        return $this->min;
    }

    public function setMin(?float $min): self
    {
        $this->min = $min;

        return $this;
    }

    public function getMax(): ?float
    {
        return $this->max;
    }

    public function setMax(?float $max): self
    {
        $this->max = $max;

        return $this;
    }

    public function getCondition(): ?CartCondition
    {
        return $this->condition;
    }

    public function setCondition(?CartCondition $condition): self
    {
        // unset the owning side of the relation if necessary
        if ($condition === null && $this->condition !== null) {
            $this->condition->setPrices(null);
        }

        // set the owning side of the relation if necessary
        if ($condition !== null && $condition->getPrices() !== $this) {
            $condition->setPrices($this);
        }

        $this->condition = $condition;

        return $this;
    }
}
