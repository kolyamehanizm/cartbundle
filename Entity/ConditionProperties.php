<?php

namespace becompact\CartBundle\Entity;

use becompact\CartBundle\Repository\ConditionPropertiesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ConditionPropertiesRepository::class)
 * @ORM\Table(name="cart_condition_properties", indexes={
 *     @ORM\Index(name="cart_condition_properties_id_idx", columns={"id"}),
 *     @ORM\Index(name="cart_condition_properties_rule_idx", columns={"condition_id"}),
 * })
 */
class ConditionProperties
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", name="condition_id", nullable=true)
     */
    private $conditionId;

    /**
     * @ORM\Column(type="integer", name="property_id", nullable=true)
     */
    private $propertyId;

    /**
     * @ORM\Column(type="string", name="property_code", nullable=true)
     */
    private $propertyCode;

    /**
     * @ORM\Column(type="string", name="term", length=5, nullable=true)
     */
    private $term;

    /**
     * @ORM\Column(type="json", name="property_values", nullable=true)
     */
    private $propertyValues = [];

    /**
     * @ORM\ManyToOne(targetEntity=CartCondition::class, inversedBy="properties")
     * @ORM\JoinColumn(name="condition_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $condition;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getConditionId(): ?int
    {
        return $this->conditionId;
    }

    public function setConditionId(?int $id): self
    {
        $this->conditionId = $id;
        return $this;
    }

    public function getPropertyId(): ?int
    {
        return $this->propertyId;
    }

    public function setPropertyId(?int $propertyId): self
    {
        $this->propertyId = $propertyId;

        return $this;
    }

    public function getPropertyCode(): ?string
    {
        return $this->propertyCode;
    }

    public function setPropertyCode(?string $propertyCode): self
    {
        $this->propertyCode = $propertyCode;

        return $this;
    }

    public function getTerm(): ?string
    {
        return $this->term;
    }

    public function setTerm(?string $term): self
    {
        $this->term = $term;

        return $this;
    }

    public function getPropertyValues(): ?array
    {
        return $this->propertyValues;
    }

    public function setPropertyValues(?array $propertyValues): self
    {
        $this->propertyValues = $propertyValues;

        return $this;
    }

    public function getCondition(): ?CartCondition
    {
        return $this->condition;
    }

    public function setCondition(?CartCondition $condition): self
    {
        $this->condition = $condition;
        return $this;
    }
}
