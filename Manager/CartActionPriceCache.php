<?php


namespace becompact\CartBundle\Manager;


use becompact\CartBundle\Entity\CartActionPrice;
use becompact\CartBundle\Model\CartActionPriceModel;
use becompact\Redis\Service\RedisService;

class CartActionPriceCache
{
    private RedisService $redis;
    private CartActionPriceSerializer $serializer;

    private string $prefix = "";

    const TTL = 3600;

    public function __construct(RedisService $redis, CartActionPriceSerializer $serializer) {
        $this->redis = $redis;
        $this->serializer = $serializer;
    }

    /**
     * Префикс для индекса в redis
     *
     * @param string|null $prefix
     */
    public function setPrefix(?string $prefix) {
        if (is_string($prefix)) {
            $this->prefix = $prefix;
        }
    }

    /**
     * Сохранение условия в redis
     *
     * @param $action
     * @return CartActionPriceModel|null
     */
    public function save($action): ?CartActionPriceModel
    {
        if ($action instanceof CartActionPrice) {
            $id = $action->getId();
            $data = $this->serializer->serialize($action);
            if ($data && $id) {
                $this->redis->set($this->getIndex($id), $data, $this::TTL);
                return (new CartActionPriceModel())->fromJson($data);
            }
        } elseif ($action instanceof CartActionPriceModel) {
            $id = $action->getId();
            $data = $action->toJson();
            if ($data && $id) {
                $this->redis->set($this->getIndex($id), $data, $this::TTL);
                return $action;
            }
        }
        return null;
    }

    /**
     * Загрузка действия из redis
     *
     * @param int|null $id
     * @return CartActionPriceModel|null
     */
    public function get(?int $id): ?CartActionPriceModel
    {
        if ($id) {
            $index = $this->getIndex($id);
            if ($this->redis->exists($index)) {
                $data = $this->redis->get($index);
                if ($data) {
                    return new CartActionPriceModel(json_decode($data, true));
                }
            }
        }
        return null;
    }

    /**
     * Удалить действие из redis
     *
     * @param int|null $id
     * @return bool
     */
    public function delete(?int $id): bool
    {
        $index = $this->getIndex($id);
        if ($this->redis->exists($index)) {
            return $this->redis->delete($index);
        }
        return false;
    }

    /**
     * @param $id
     * @return string
     */
    private function getIndex($id): string
    {
        return $this->prefix . 'cart_action_price.id_' . $id;
    }
}
