<?php


namespace becompact\CartBundle\Manager;


use becompact\CartBundle\Entity\CartActionPrice;
use becompact\CartBundle\Model\CartActionPriceModel;
use becompact\CartBundle\Utils\BundleValidator;
use becompact\Common\MessageTrait;

class CartActionPriceManager
{
    use MessageTrait;

    private BundleValidator $validator;
    private CartActionPriceCache $cache;
    private CartActionPriceRepo $repo;

    public function __construct(
        BundleValidator $validator,
        CartActionPriceCache $cache,
        CartActionPriceRepo $repo,
        $prefix = null
    ) {
        $this->validator = $validator;
        $this->cache = $cache;
        $this->repo = $repo;

        if (is_string($prefix)) {
            $this->cache->setPrefix($prefix);
        }
    }

    /**
     * Получить действие по идентификатору
     * @param int|null $id
     * @param bool $fast
     * @return CartActionPriceModel|null
     */
    public function getAction(?int $id, bool $fast): ?CartActionPriceModel
    {
        if (!$this->validator->isValidId($id)) {
            $this->setMessage('Неверный идентификатор');
            return null;
        }

        $action = $this->cache->get($id);
        if ($action) { return $action; }
        if ($fast) { return null; }
        $action = $this->repo->getActionById($id);
        if ($action) {
            return $this->cache->save($action);
        }
        return null;
    }


    /**
     * Редактирование действия
     * @param null $parameters
     * @return CartActionPriceModel|null
     */
    public function editAction($parameters = null): ?CartActionPriceModel
    {
        if (!is_array($parameters)) { return null; }

        $id = $parameters['id'] ?? null;
        $name = $parameters['name'] ?? null;
        $treeId = $parameters['tree_id'] ?? $parameters['treeId'] ?? null;
        $free = $parameters['free'] ?? null;

        $discount = $parameters['discount'] ?? null;
        $percent = $parameters['percent'] ?? null;

        $action = $this->validator->isValidId($id) ? $this->repo->getActionById((int)$id) : null;
        if (!$action) {
            $action =  new CartActionPrice();
        }

        //Наименование
        if (!is_null($name) && $this->validator->isValidEmptyText($name)) {
            if (!$name) {
                $action->setName(null);
            } else if ($this->validator->isValidText($name) && ($action->getName() !== (string)$name)) {
                $action->setName((string)$name);
            }
        }

        //Идентификатор дерева
        if (!is_null($treeId) && $this->validator->isValidInt($treeId)) {
            if (!$treeId) {
                $action->setTreeId(null);
            } else if ($this->validator->isValidId($treeId) && ($action->getTreeId() !== (int)$treeId)) {
                $action->setTreeId((int)$treeId);
            }
        }

        //Бесплатно
        if (!is_null($free) && $this->validator->isValidBool($free)) {
            $action->setFree($free === true);
        }

        //Скидка
        if (!is_null($discount) && $this->validator->isValidInt($discount)) {
            if (!$discount) {
                $action->setDiscount(0);
            } else if ($this->validator->isValidId($discount) && ($action->getDiscount() !== (int)$discount)) {
                $action->setDiscount((int)$discount);
            }
        }

        //Процент
        if (!is_null($percent) && $this->validator->isValidBool($percent)) {
            $action->setPercent($percent === true);
        }

        $this->repo->saveAction($action);
        return $this->cache->save($action);
    }



    /**
     * Удаление действия
     * @param $id
     * @return bool
     */
    public function deleteAction($id): bool
    {
        if (!$this->validator->isValidId($id)) {
            $this->setMessage('Неверный идентификатор');
            return false;
        }

        $action = $this->repo->getActionById($id);
        if (!$action) {
            $this->setMessage('Нет действия с данным идентификатором');
            return false;
        }

        $this->cache->delete($id);
        $this->repo->deleteAction($action);
        return true;
    }

}
