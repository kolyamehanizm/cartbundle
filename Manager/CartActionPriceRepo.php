<?php


namespace becompact\CartBundle\Manager;


use becompact\CartBundle\Entity\CartActionPrice;
use Doctrine\ORM\EntityManagerInterface;

class CartActionPriceRepo
{
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @param array|null $params
     * @return CartActionPrice|null
     */
    private function findByParams(?array $params = null): ?CartActionPrice
    {
        return is_array($params) ?  $this->em->getRepository(CartActionPrice::class)->findOneBy($params) : null;
    }

    /**
     * @param int|null $id
     * @param null $active
     * @return CartActionPrice|null
     */
    public function getActionById(?int $id): ?CartActionPrice
    {
        return $this->findByParams([ 'id' => $id ]);
    }

    /**
     * @param $id
     * @param null $active
     * @return array
     */
    public function getActionsByTreeId($id): array
    {
        return $this->em->getRepository(CartActionPrice::class)->findBy(["treeId" => (int)$id]);
    }

    /**
     * @param CartActionPrice $action
     */
    public function saveAction(CartActionPrice &$action)
    {
        $this->em->persist($action);
        $this->em->flush();
    }

    /**
     * @param CartActionPrice $action
     */
    public function deleteAction(CartActionPrice $action)
    {
        $this->em->remove($action);
        $this->em->flush();
    }
}
