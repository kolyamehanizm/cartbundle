<?php


namespace becompact\CartBundle\Manager;


use becompact\CartBundle\Entity\CartActionPrice;
use becompact\CartBundle\Normalizers\CartActionPriceNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;

class CartActionPriceSerializer
{
    public function serialize(?CartActionPrice $action): ?string
    {
        if ($action instanceof CartActionPrice) {
            $encoders = [new JsonEncoder()];
            $normalizers = [
                new CartActionPriceNormalizer(),
            ];

            $serializer = new Serializer($normalizers, $encoders);
            return $serializer->serialize($action, 'json');
        }
        return null;
    }
}
