<?php

namespace becompact\CartBundle\Manager;

use becompact\CartBundle\Entity\CartCondition;
use becompact\CartBundle\Model\CartConditionModel;
use becompact\Redis\Service\RedisService;

class CartConditionCache
{
    private RedisService $redis;
    private CartConditionSerializer $serializer;

    private string $prefix = "";

    const TTL = 3600;

    public function __construct(RedisService $redis, CartConditionSerializer $serializer) {
        $this->redis = $redis;
        $this->serializer = $serializer;
    }

    /**
     * Префикс для индекса в redis
     *
     * @param string|null $prefix
     */
    public function setPrefix(?string $prefix) {
        if (is_string($prefix)) {
            $this->prefix = $prefix;
        }
    }

    /**
     * Сохранение условия в redis
     *
     * @param $condition
     * @return CartConditionModel|null
     */
    public function save($condition): ?CartConditionModel
    {
        if ($condition instanceof CartCondition) {
            $id = $condition->getId();
            $data = $this->serializer->serialize($condition);
            if ($data && $id) {
                $this->redis->set($this->getIndex($id), $data, $this::TTL);
                return (new CartConditionModel())->fromJson($data);
            }
        } elseif ($condition instanceof CartConditionModel) {
            $id = $condition->getId();
            $data = $condition->toJson();
            if ($data && $id) {
                $this->redis->set($this->getIndex($id), $data, $this::TTL);
                return $condition;
            }
        }
        return null;
    }

    /**
     * Загрузка условия из redis
     *
     * @param int|null $id
     * @return CartConditionModel|null
     */
    public function get(?int $id): ?CartConditionModel
    {
        if ($id) {
            $index = $this->getIndex($id);
            if ($this->redis->exists($index)) {
                $data = $this->redis->get($index);
                if ($data) {
                    return new CartConditionModel(json_decode($data, true));
                }
            }
        }
        return null;
    }

    /**
     * Удалить условие из redis
     *
     * @param int|null $id
     * @return bool
     */
    public function delete(?int $id): bool
    {
        $index = $this->getIndex($id);
        if ($this->redis->exists($index)) {
            return $this->redis->delete($index);
        }
        return false;
    }

    /**
     * @param $id
     * @return string
     */
    private function getIndex($id): string
    {
        return $this->prefix . 'cart_condition.id_' . $id;
    }
}
