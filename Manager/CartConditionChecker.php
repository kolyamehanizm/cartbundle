<?php

namespace becompact\CartBundle\Manager;

use becompact\CartBundle\Entity\CartCondition;
use becompact\CartBundle\Entity\ConditionArticles;
use becompact\CartBundle\Entity\ConditionBrands;
use becompact\CartBundle\Entity\ConditionCategories;
use becompact\CartBundle\Entity\ConditionCounts;
use becompact\CartBundle\Entity\ConditionDeliveries;
use becompact\CartBundle\Entity\ConditionPrices;
use becompact\CartBundle\Entity\ConditionProperties;
use becompact\CartBundle\Model\CartConditionModel;
use becompact\CartBundle\Model\ConditionArticlesModel;
use becompact\CartBundle\Model\ConditionBrandsModel;
use becompact\CartBundle\Model\ConditionCategoriesModel;
use becompact\CartBundle\Model\ConditionCountsModel;
use becompact\CartBundle\Model\ConditionDeliveriesModel;
use becompact\CartBundle\Model\ConditionPricesModel;
use becompact\CartBundle\Model\ConditionPropertiesModel;
use becompact\GoodBundle\Model\GoodModel;
use becompact\GoodBundle\Model\OptionModel;

class CartConditionChecker
{
    private int $priceId = 1;

    public function __construct($price = null) {
        if (is_numeric($price)) {
            $this->priceId = (int)$price;
        }
    }

    private function checkArticles($rule, GoodModel &$good): ?bool
    {
        if (!($rule instanceof ConditionArticles || $rule instanceof ConditionArticlesModel)) { return null; }

        $include = $rule->getInclude();
        $exclude = $rule->getExclude();

        $incStatus = true;
        $excStatus = true;
        if (is_array($include) && count($include)) {
            $incStatus = in_array($good->getArticle(), $include);
        }

        if (is_array($exclude) && count($exclude)) {
            $excStatus = !in_array($good->getArticle(), $exclude);
        }

        return $incStatus && $excStatus;
    }

    private function checkBrand($rule, GoodModel &$good): ?bool
    {
        if (!($rule instanceof ConditionBrands || $rule instanceof ConditionBrandsModel)) { return null; }

        if (!$good->getBrand()) { return null; };

        $include = $rule->getInclude();
        $exclude = $rule->getExclude();

        $incStatus = true;
        $excStatus = true;
        if (is_array($include) && count($include)) {
            $incStatus = in_array($good->getBrand(), $include);
        }

        if (is_array($exclude) && count($exclude)) {
            $excStatus = !in_array($good->getBrand(), $exclude);
        }

        return $incStatus && $excStatus;
    }

    private function checkCategories($rule, GoodModel &$good): ?bool
    {
        if (!($rule instanceof ConditionCategories || $rule instanceof ConditionCategoriesModel)) { return null; }

        $categories = $good->getCategories();

        $include = $rule->getInclude();
        $exclude = $rule->getExclude();

        $incStatus = true;
        $excStatus = true;
        if (is_array($include) && count($include)) {
            $incStatus = count(array_intersect($categories, $include)) > 0;
        }

        if (is_array($exclude) && count($exclude)) {
            $excStatus = count(array_intersect($categories, $exclude)) == 0;
        }

        return $incStatus && $excStatus;
    }

    private function checkCount($rule, GoodModel &$good): ?int
    {
        if (!($rule instanceof ConditionCounts || $rule instanceof ConditionCountsModel)) { return null; }

        $min = $rule->getMin() ?? 0;
        $multiply = $rule->getMultiple() === true;

        $cnt = $good->getCount() ?? 1;
        if ($cnt < $min) { return false; }

        if ($multiply) {
            return $min > 0 ? intdiv($cnt, $min) : $cnt;
        }

        return 1;
    }

    private function checkDelivery($rule, GoodModel &$good): ?bool
    {
        if (!($rule instanceof ConditionDeliveries || $rule instanceof ConditionDeliveriesModel)) { return null; }

        $goodDeliveries = $good->getDeliveries();
        if (!count($goodDeliveries)) { return false; }

        $cartDeliveries = $rule->getDeliveries();
        if (!is_array($cartDeliveries) || !count($cartDeliveries)) { return true; }

        $ids = array_keys($goodDeliveries);

        return count(array_intersect($ids, $cartDeliveries)) > 0;
    }

    private function checkPrice($rule, GoodModel &$good): ?bool
    {
        if (!($rule instanceof ConditionPrices || $rule instanceof ConditionPricesModel)) { return null; }

        $price = $good->getPrice($this->priceId);

        if (!$price) { return false; }

        $min = $rule->getMin() ?? 0;
        $max = $rule->getMax() ?? $price;

        if ($price >= $min && $price <= $max) {
            return true;
        }

        return false;
    }

    private function checkProperty($rule, GoodModel &$good): ?bool
    {
        if (!($rule instanceof ConditionProperties || $rule instanceof ConditionPropertiesModel)) { return null; }

        // $propId = $rule->getPropertyId();
        $propCode = $rule->getPropertyCode();
        $term = $rule->getTerm();
        $values = $rule->getPropertyValues();

        if (/*!$propId*/ !$propCode || !$term || !is_array($values) || !count($values)) { return null; }

        $property = null;
        //$property = $properties[$propId] ?? null;

        $properties = $good->getOptions();
        foreach ($properties as $prop) {
            if ($prop->getCode() != $propCode) {
                continue;
            }
            $property = $prop;
            break;
        }

        if (!($property instanceof OptionModel)) { return false; }

        $propertyType = $property->getType();
        $propertyValues = $property->getValue();

        if (is_null($propertyType) || is_null($propertyValues)) {
            //echo "нет свойств для сравнения\n";
            return false;
        }

        switch($term) {
            case '>':
                $value = $values[0] ?? null;
                $propertyValue = null;

                if (is_numeric($value)) {
                    if ($propertyType === 'd' || $propertyType === 's') {
                        $propertyValue = $propertyValues;
                    }
                    if (is_numeric($propertyValue) && ($propertyValue > $value)) {
                        return true;
                    }
                }
                break;
            case '<':
                $value = $values[0] ?? null;
                $propertyValue = null;

                if (is_numeric($value)) {
                    if ($propertyType === 'd' || $propertyType === 's') {
                        $propertyValue = $propertyValues;
                    }
                    if (is_numeric($propertyValue) && ($propertyValue < $value)) {
                        return true;
                    }
                }
                break;
            case '>=':
                $value = $values[0] ?? null;
                $propertyValue = null;

                if (is_numeric($value)) {
                    if ($propertyType === 'd' || $propertyType === 's') {
                        $propertyValue = $propertyValues;
                    }
                    if (is_numeric($propertyValue) && ($propertyValue >= $value)) {
                        return true;
                    }
                }
                break;
            case '<=':
                $value = $values[0] ?? null;
                $propertyValue = null;

                if (is_numeric($value)) {
                    if ($propertyType === 'd' || $propertyType === 's') {
                        $propertyValue = $propertyValues;
                    }
                    if (is_numeric($propertyValue) && ($propertyValue <= $value)) {
                        return true;
                    }
                }
                break;
            case '=':
                $value = $values[0] ?? null;
                $propertyValue = $propertyValues[0] ?? null;

                if ($propertyType === 'd' || $propertyType === 's') {
                    $propertyValue = $propertyValues;
                }
                if (!is_null($value) && !is_null($propertyValue) && ($propertyValue == $value)) {
                    return true;
                }
                break;
            case '!=':
                $value = $values[0] ?? null;
                $propertyValue = $propertyValues[0] ?? null;

                if ($propertyType === 'd' || $propertyType === 's') {
                    $propertyValue = $propertyValues;
                }
                if (!is_null($value) && !is_null($propertyValue) && ($propertyValue != $value)) {
                    return true;
                }
                break;
            case 'in':
                if ($propertyType === 'd' || $propertyType === 's') {
                    $propertyValues = [(string)$propertyValues];
                }

                $arr = array_intersect($values, $propertyValues);
                return count($arr) > 0;
            case '!in':
                if ($propertyType === 'd' || $propertyType === 's') {
                    $propertyValues = [(string)$propertyValues];
                }

                $arr = array_intersect($values, $propertyValues);
                return count($arr) == 1;
        }

        return false;
    }

    public function check($rule, ?GoodModel &$good): ?bool
    {
        if (!($good instanceof GoodModel)) { return null; }
        if (!($rule instanceof CartCondition || $rule instanceof CartConditionModel)) { return null; }

        if (!$rule->getActive()) {
            return null;
        }

        foreach ($rule->getProperties() as $property) {
            $status = $this->checkProperty($property, $good);
            if (!is_null($status) && !$status) {
                return false;
            }
        }

        $status = $this->checkPrice($rule->getPrices(), $good);
        if (!is_null($status) && !$status) {
            return false;
        }

        $status = $this->checkDelivery($rule->getDeliveries(), $good);
        if (!is_null($status) && !$status) {
            return false;
        }

        $status = $this->checkCount($rule->getCounts(), $good);
        if (!is_null($status) && !$status) {
            return false;
        }

        $status = $this->checkCategories($rule->getCategories(), $good);
        if (!is_null($status) && !$status) {
            return false;
        }

        $status = $this->checkBrand($rule->getBrands(), $good);
        if (!is_null($status) && !$status) {
            return false;
        }

        $status = $this->checkArticles($rule->getArticles(), $good);
        if (!is_null($status) && !$status) {
            return false;
        }

        return true;
    }
}
