<?php

namespace becompact\CartBundle\Manager;

use becompact\CartBundle\Entity\CartCondition;
use becompact\CartBundle\Entity\ConditionArticles;
use becompact\CartBundle\Entity\ConditionBrands;
use becompact\CartBundle\Entity\ConditionCategories;
use becompact\CartBundle\Entity\ConditionCounts;
use becompact\CartBundle\Entity\ConditionDeliveries;
use becompact\CartBundle\Entity\ConditionPrices;
use becompact\CartBundle\Entity\ConditionProperties;
use becompact\CartBundle\Model\CartConditionModel;

use becompact\Common\MessageTrait;
use becompact\CartBundle\Utils\BundleValidator;

class CartConditionManager
{
    use MessageTrait;

    private BundleValidator $validator;
    private CartConditionCache $cache;
    private CartConditionRepo $repo;

    public function __construct(
        BundleValidator $validator,
        CartConditionCache $cache,
        CartConditionRepo $repo,
        $prefix = null
    ) {
        $this->validator = $validator;
        $this->cache = $cache;
        $this->repo = $repo;

        if (is_string($prefix)) {
            $this->cache->setPrefix($prefix);
        }
    }

    /**
     * Получить условие по идентификатору
     *
     * @param int|null $id
     * @param bool $fast
     * @return CartConditionModel|null
     */
    public function getCondition(?int $id, bool $fast): ?CartConditionModel
    {
        if (!$this->validator->isValidId($id)) {
            $this->setMessage('Неверный идентификатор');
            return null;
        }

        $condition = $this->cache->get($id);
        if ($condition) { return $condition; }
        if ($fast) { return null; }
        $condition = $this->repo->getConditionById($id);
        if ($condition) {
            return $this->cache->save($condition);
        }
        return null;
    }

    /**
     * Редактирование условия
     *
     * @param null $parameters
     * @return CartConditionModel|null
     */
    public function editCondition($parameters = null): ?CartConditionModel
    {
        if (!is_array($parameters)) { return null; }

        $id = $parameters['id'] ?? null;
        $name = $parameters['name'] ?? null;
        $treeId = $parameters['tree_id'] ?? $parameters['treeId'] ?? null;
        $active = $parameters['active'] ?? null;

        $articles = $parameters['articles'] ?? null;
        $brands = $parameters['brands'] ?? null;
        $categories = $parameters['categories'] ?? null;
        $counts = $parameters['counts'] ?? null;
        $deliveries = $parameters['deliveries'] ?? null;
        $prices = $parameters['prices'] ?? null;
        $properties = $parameters['properties'] ?? null;

        $rule = $this->validator->isValidId($id) ? $this->repo->getConditionById((int)$id) : null;
        if (!$rule) {
            $rule =  new CartCondition();
        }

        //Наименование
        if (!is_null($name) && $this->validator->isValidEmptyText($name)) {
            if (!$name) {
                $rule->setName(null);
            } else if ($this->validator->isValidText($name) && ($rule->getName() !== (string)$name)) {
                $rule->setName((string)$name);
            }
        }

        //Идентификатор дерева
        if (!is_null($treeId) && $this->validator->isValidInt($treeId)) {
            if (!$treeId) {
                $rule->setTreeId(null);
            } else if ($this->validator->isValidId($treeId) && ($rule->getTreeId() !== (int)$treeId)) {
                $rule->setTreeId((int)$treeId);
            }
        }

        //Активность
        if (!is_null($active) && $this->validator->isValidBool($active)) {
            $rule->setActive($active === true);
        }

        //артикулы товара для правила
        if (is_null($articles)) {
            //удаляем артикулы товара у правила
            if ($ruleArticles = $rule->getArticles()) {
                $ruleArticles->setCondition(null);
                $rule->setArticles(null);

                $this->repo->remove($ruleArticles);
            }
        } else {
            if ($this->validator->isValidArray($articles)) {
                $include = $articles['include'] ?? [];
                $exclude = $articles['exclude'] ?? [];

                if ($this->validator->isValidArray($include)) {
                    $arr = [];
                    foreach ($include as $article) {
                        if (is_numeric($article)) {
                            $arr[] = (int)$article;
                        }
                    }
                    $include = $arr;
                } else { $include = []; }

                if ($this->validator->isValidArray($exclude)) {
                    $arr = [];
                    foreach ($exclude as $article) {
                        if (is_numeric($article)) {
                            $arr[] = (int)$article;
                        }
                    }
                    $exclude = $arr;
                } else { $exclude = []; }

                $ruleArticles = $rule->getArticles();
                if (!$ruleArticles) {
                    $ruleArticles = new ConditionArticles();
                }

                $ruleArticles
                    ->setInclude($include)
                    ->setExclude($exclude);

                $rule->setArticles($ruleArticles);
            }
        }

        //бренды товара для правила
        if (is_null($brands)) {
            //удаляем артикулы товара у правила
            if ($ruleBrands = $rule->getBrands()) {
                $ruleBrands->setCondition(null);
                $rule->setBrands(null);

                $this->repo->remove($ruleBrands);
            }
        } else {
            if ($this->validator->isValidArray($brands)) {
                $include = $brands['include'] ?? [];
                $exclude = $brands['exclude'] ?? [];

                if ($this->validator->isValidArray($include)) {
                    $arr = [];
                    foreach ($include as $brand) {
                        if (is_string($brand) && $brand) {
                            $arr[] = $brand;
                        }
                    }
                    $include = $arr;
                } else { $include = []; }

                if ($this->validator->isValidArray($exclude)) {
                    $arr = [];
                    foreach ($exclude as $brand) {
                        if (is_string($brand) && $brand) {
                            $arr[] = $brand;
                        }
                    }
                    $exclude = $arr;
                } else { $exclude = []; }

                $ruleBrands = $rule->getBrands();
                if (!$ruleBrands) {
                    $ruleBrands = new ConditionBrands();
                }

                $ruleBrands
                    ->setInclude($include)
                    ->setExclude($exclude);

                $rule->setBrands($ruleBrands);
            }
        }

        //категории товара для правила
        if (is_null($categories)) {
            //удаляем категории товара у правила
            if ($ruleCategories = $rule->getCategories()) {
                $rule->setCategories(null);

                $ruleCategories->setCondition(null);
                $this->repo->remove($ruleCategories);
            }
        } else {
            if ($this->validator->isValidArray($categories)) {
                $include = $categories['include'] ?? null;
                $exclude = $categories['exclude'] ?? null;

                if ($this->validator->isValidArray($include)) {
                    $arr = [];
                    foreach ($include as $category) {
                        if (is_numeric($category)) {
                            $arr[] = (int)$category;
                        }
                    }
                    $include = count($arr) ? $arr : null;
                }

                if ($this->validator->isValidArray($exclude)) {
                    $arr = [];
                    foreach ($exclude as $category) {
                        if (is_numeric($category)) {
                            $arr[] = (int)$category;
                        }
                    }
                    $exclude = count($arr) ? $arr : null;
                }

                $ruleCategories = $rule->getCategories();
                if (!$ruleCategories) {
                    $ruleCategories = new ConditionCategories();
                }

                $ruleCategories
                    ->setInclude($include)
                    ->setExclude($exclude);

                $rule->setCategories($ruleCategories);
            }
        }

        //ограничения по количеству товара для правила
        if (is_null($counts)) {
            //удаляем ограничения по количеству товара для правила
            if ($ruleCounts = $rule->getCounts()) {
                $rule->setCounts(null);

                $ruleCounts->setCondition(null);
                $this->repo->remove($ruleCounts);
            }
        } else {
            if ($this->validator->isValidArray($counts)) {
                $min = $counts['min'] ?? null;
                $multiple = $counts['multiple'] ?? null;

                $ruleCounts = $rule->getCounts();
                if (!$ruleCounts) {
                    $ruleCounts = new ConditionCounts();
                }

                $ruleCounts
                    ->setMin(null)
                    ->setMultiple(null);

                if ($this->validator->isValidInt($min)) {
                    $ruleCounts->setMin((int)$min);
                }

                if ($this->validator->isValidBool($multiple)) {
                    $ruleCounts->setMultiple($multiple === true);
                }

                $rule->setCounts($ruleCounts);
            }
        }

        //идентификаторы доставок
        if (is_null($deliveries)) {
            //удаляем доставки
            if ($ruleDeliveries = $rule->getDeliveries()) {
                $ruleDeliveries->setCondition(null);
                $rule->setDeliveries(null);

                $this->repo->remove($ruleDeliveries);
            }
        } else {
            if ($this->validator->isValidArray($deliveries)) {
                $values = $deliveries['deliveries'] ?? null;

                if ($this->validator->isValidArray($values)) {
                    $arr = [];
                    foreach ($values as $value) {
                        if (is_numeric($value)) {
                            $arr[] = (int)$value;
                        }
                    }
                    $values = count($arr) ? $arr : null;
                }

                $ruleDeliveries = $rule->getDeliveries();
                if (!$ruleDeliveries) {
                    $ruleDeliveries = new ConditionDeliveries();
                }

                $ruleDeliveries->setDeliveries($values);

                $rule->setDeliveries($ruleDeliveries);
            }
        }

        //ограничение по цене товара для правила
        if (is_null($prices)) {
            //удаляем ограничения по количеству товара для правила
            if ($rulePrices = $rule->getPrices()) {
                $rule->setPrices(null);

                $rulePrices->setCondition(null);
                $this->repo->remove($rulePrices);
            }
        } else {
            if ($this->validator->isValidArray($prices)) {
                $min = $counts['min'] ?? null;
                $max = $counts['max'] ?? null;

                $rulePrices = $rule->getPrices();
                if (!$rulePrices) {
                    $rulePrices = new ConditionPrices();
                }

                $rulePrices
                    ->setMin(null)
                    ->setMax(null);

                if ($this->validator->isValidFloat($min)) {
                    $rulePrices->setMin((float)$min);
                }

                if ($this->validator->isValidFloat($max)) {
                    $rulePrices->setMax((float)$min);
                }

                $rule->setPrices($rulePrices);
            }
        }

        //ограничения по свойствам
        if (is_null($properties)) {
            $ruleProperties = $rule->getProperties();
            foreach ($ruleProperties as $ruleProperty) {
                $rule->removeProperty($ruleProperty);
            }
        } else {
            if ($this->validator->isValidArrayValues($properties)) {
                $ruleProperties = $rule->getProperties();

                foreach ($properties as $property) {
                    $id = $property['id'] ?? null;
                    $propertyId = $property['property_id'] ?? $propertyp['propertyId'] ?? null;
                    $propertyCode = $property['property_code'] ?? $propertyp['propertyCode'] ?? null;
                    $term = $property['term'] ?? null;
                    $propertyValues = $property['property_values'] ?? $property['propertyValues'] ?? null;

                    if (/*!$this->validator->isValidId($propertyId) ||*/
                        !$this->validator->isValidText($propertyCode) ||
                        !$this->validator->isValidText($term) ||
                        !$this->validator->isValidArrayValues($propertyValues)
                    ) {
                        //удалить свойство если указан id и не заполнены значения
                        if ($this->validator->isValidId($id)) {
                            foreach ($ruleProperties as $ruleProperty) {
                                if ($ruleProperty->getId() === (int)$id) {
                                    $rule->removeProperty($ruleProperty);
                                }
                            }
                        }
                        continue;
                    }
                    if (!in_array($term, ['>','<','>=','<=','=','!=','in','!in'])) {
                        continue;
                    }

                    if ($this->validator->isValidId($id)) {
                        //существует свойство
                        foreach ($ruleProperties as $ruleProperty) {
                            if ($ruleProperty->getId() === (int)$id) {
                                $ruleProperty->setPropertyId($propertyId ?: null)
                                    ->setPropertyCode($propertyCode)
                                    ->setTerm($term)
                                    ->setPropertyValues($propertyValues);
                            }
                        }
                    } else {
                        //новое свойство
                        $ruleProperty = (new ConditionProperties())
                            ->setPropertyId($propertyId ?: null)
                            ->setPropertyCode($propertyCode)
                            ->setTerm($term)
                            ->setPropertyValues($propertyValues);

                        $rule->addProperty($ruleProperty);
                    }
                }
            }
        }

        $this->repo->saveCondition($rule);
        return $this->cache->save($rule);
    }

    /**
     * Удаление условия
     *
     * @param $id
     * @return bool
     */
    public function deleteCondition($id): bool
    {
        if (!$this->validator->isValidId($id)) {
            $this->setMessage('Неверный идентификатор');
            return false;
        }

        $rule = $this->repo->getConditionById($id);
        if (!$rule) {
            $this->setMessage('Нет правила с данным идентификатором');
            return false;
        }

        $this->cache->delete($id);
        $this->repo->deleteCondition($rule);
        return true;
    }
}
