<?php

namespace becompact\CartBundle\Manager;

use becompact\CartBundle\Entity\CartCondition;
use Doctrine\ORM\EntityManagerInterface;

class CartConditionRepo
{
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @param array|null $params
     * @return CartCondition|null
     */
    private function findConditionByParams(?array $params = null): ?CartCondition
    {
        return is_array($params) ?  $this->em->getRepository(CartCondition::class)->findOneBy($params) : null;
    }


    /**
     * @param int|null $id
     * @param null $active
     * @return CartCondition|null
     */
    public function getConditionById(?int $id, $active = null): ?CartCondition
    {
        $params = [ 'id' => $id ];
        if (is_bool($active)) { $params['active'] = $active === true; }
        return $this->findConditionByParams($params);
    }

    /**
     * @param $id
     * @param null $active
     * @return array
     */
    public function getConditionsByTreeId($id, $active = null): array
    {
        $params = ["treeId" => (int)$id ];
        if (is_bool($active)) { $params['active'] = $active === true; }
        return $this->em->getRepository(CartCondition::class)->findBy($params);
    }

    /**
     * @param $ids
     * @param null $active
     * @return array|null
     */
    public function getConditionsByIds($ids, $active = null): ?array
    {
        if (!is_array($ids)) { return null; }
        $treeIds = [];
        foreach ($ids as $id) {
            if (!is_numeric($id)) { continue; }
            $treeIds[] = (int)$id;
        }
        if (!count($treeIds)) { return null; }
        $params = ["treeId" => $treeIds ];
        if (is_bool($active)) { $params['active'] = $active === true; }
        return $this->em->getRepository(CartCondition::class)->findBy($params);
    }

    /**
     * @param CartCondition $rule
     */
    public function saveCondition(CartCondition &$rule)
    {
        $this->em->persist($rule);
        $this->em->flush();
    }

    /**
     * @param CartCondition $rule
     */
    public function deleteCondition(CartCondition $rule)
    {
        $this->em->remove($rule);
        $this->em->flush();
    }

    /**
     * @param $object
     */
    public function persist($object)
    {
        $this->em->persist($object);
    }

    /**
     * @param $object
     */
    public function remove($object) {
        $this->em->remove($object);
    }

}
