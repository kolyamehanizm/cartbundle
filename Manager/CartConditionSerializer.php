<?php

namespace becompact\CartBundle\Manager;

use becompact\CartBundle\Entity\CartCondition;

use becompact\CartBundle\Normalizers\CartConditionNormalizer;
use becompact\CartBundle\Normalizers\ConditionArticlesNormalizer;
use becompact\CartBundle\Normalizers\ConditionBrandsNormalizer;
use becompact\CartBundle\Normalizers\ConditionCategoriesNormalizer;
use becompact\CartBundle\Normalizers\ConditionCountsNormalizer;
use becompact\CartBundle\Normalizers\ConditionDeliveriesNormalizer;
use becompact\CartBundle\Normalizers\ConditionPricesNormalizer;
use becompact\CartBundle\Normalizers\ConditionPropertiesNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;

class CartConditionSerializer
{
    public function serialize(?CartCondition $rule): ?string
    {
        if ($rule instanceof CartCondition) {
            $encoders = [new JsonEncoder()];
            $normalizers = [
                new CartConditionNormalizer(),
                new ConditionArticlesNormalizer(),
                new ConditionBrandsNormalizer(),
                new ConditionCategoriesNormalizer(),
                new ConditionCountsNormalizer(),
                new ConditionDeliveriesNormalizer(),
                new ConditionPricesNormalizer(),
                new ConditionPropertiesNormalizer(),
            ];

            $serializer = new Serializer($normalizers, $encoders);
            return $serializer->serialize($rule, 'json');
        }
        return null;
    }
}
