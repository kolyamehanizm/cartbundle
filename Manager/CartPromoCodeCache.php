<?php

namespace becompact\CartBundle\Manager;

use becompact\CartBundle\Entity\CartPromoCode;
use becompact\CartBundle\Model\CartPromoCodeModel;
use becompact\Redis\Service\RedisService;

class CartPromoCodeCache
{
    private RedisService $redis;
    private CartPromoCodeSerializer $serializer;

    private string $prefix = "";

    const TTL = 3600;

    public function __construct(RedisService $redis, CartPromoCodeSerializer $serializer) {
        $this->redis = $redis;
        $this->serializer = $serializer;
    }

    /**
     * Префикс для индекса в redis
     *
     * @param string|null $prefix
     */
    public function setPrefix(?string $prefix) {
        if (is_string($prefix)) {
            $this->prefix = $prefix;
        }
    }

    /**
     * Сохранение промокода в redis
     *
     * @param $promocode
     * @return CartPromoCodeModel|null
     */
    public function save($promocode): ?CartPromoCodeModel
    {
        if ($promocode instanceof CartPromoCode) {
            $id = $promocode->getId();
            $code = $promocode->getCode();
            $data = $this->serializer->serialize($promocode);
            if ($data && $id) {
                $this->redis->set($this->getIndex($id), $data, $this::TTL);
                $this->redis->set($this->getIndexCode($code), $id, $this::TTL);
                return (new CartPromoCodeModel())->fromJson($data);
            }
        } elseif ($promocode instanceof CartPromoCodeModel) {
            $id = $promocode->getId();
            $code = $promocode->getCode();
            $data = $promocode->toJson();
            if ($data && $id) {
                $this->redis->set($this->getIndex($id), $data, $this::TTL);
                $this->redis->set($this->getIndexCode($code), $id, $this::TTL);
                return $promocode;
            }
        }
        return null;
    }

    /**
     * Загрузка промокода из redis
     * @param $id
     * @return CartPromoCodeModel|null
     */
    public function get($id): ?CartPromoCodeModel
    {
        if (is_numeric($id)) {
            $index = $this->getIndex($id);
            if ($this->redis->exists($index)) {
                $data = $this->redis->get($index);
                if ($data) {
                    return new CartPromoCodeModel(json_decode($data, true));
                }
            }
        }
        return null;
    }

    /**
     * Загрузка промокода из redis
     * @param string|null $code
     * @return CartPromoCodeModel|null
     */
    public function getCode(?string $code): ?CartPromoCodeModel
    {
        if ($code) {
            $index = $this->getIndexCode($code);
            if ($this->redis->exists($index)) {
                return $this->get($this->redis->get($index));
            }
        }
        return null;
    }


    /**
     * Удалить промокод из redis
     * @param int|null $id
     * @return bool
     */
    public function delete(?int $id): bool
    {
        $index = $this->getIndex($id);
        if ($this->redis->exists($index)) {
            return $this->redis->delete($index);
        }
        return false;
    }

    /**
     * Удалить промокод из redis
     * @param string|null $code
     * @return bool
     */
    public function deleteCode(?string $code): bool
    {
        $index = $this->getIndexCode($code);
        if ($this->redis->exists($index)) {
            return $this->redis->delete($index);
        }
        return false;
    }


    /**
     * @param $id
     * @return string
     */
    private function getIndex($id): string
    {
        return $this->prefix . 'cart_promocode.id_' . $id;
    }

    /**
     * @param $code
     * @return string
     */
    private function getIndexCode($code): string
    {
        return $this->prefix . 'cart_promocode.code_' . $code;
    }
}
