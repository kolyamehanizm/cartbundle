<?php


namespace becompact\CartBundle\Manager;


use becompact\CartBundle\Entity\CartPromoCode;
use becompact\CartBundle\Model\CartPromoCodeModel;
use becompact\CartBundle\Utils\BundleValidator;
use becompact\Common\MessageTrait;

class CartPromoCodeManager
{
    use MessageTrait;

    private BundleValidator $validator;
    private CartPromoCodeCache $cache;
    private CartPromoCodeRepo $repo;

    public function __construct(
        BundleValidator $validator,
        CartPromoCodeCache $cache,
        CartPromoCodeRepo $repo,
        $prefix = null
    ) {
        $this->validator = $validator;
        $this->cache = $cache;
        $this->repo = $repo;

        if (is_string($prefix)) {
            $this->cache->setPrefix($prefix);
        }
    }

    /**
     * Получить промокод по идентификатору
     * @param int|null $id
     * @param bool $fast
     * @return CartPromoCodeModel|null
     */
    public function getPromoCodeById(?int $id, bool $fast): ?CartPromoCodeModel
    {
        if (!$this->validator->isValidId($id)) {
            $this->setMessage('Неверный идентификатор');
            return null;
        }

        $promocode = $this->cache->get($id);
        if ($promocode) { return $promocode; }
        if ($fast) { return null; }
        $promocode = $this->repo->getPromoCodeById($id);
        if ($promocode) {
            return $this->cache->save($promocode);
        }
        return null;
    }

    /**
     * Получить промокод по коду
     * @param $code
     * @param bool $fast
     * @return CartPromoCodeModel|null
     */
    public function getPromoCode($code, bool $fast): ?CartPromoCodeModel
    {
        if (!$this->validator->isValidText($code)) {
            $this->setMessage('Неверный промокод');
            return null;
        }

        $promocode = $this->cache->getCode($code);
        if ($promocode) { return $promocode; }
        if ($fast) { return null; }
        $promocode = $this->repo->getPromoCode($code);
        if ($promocode) {
            return $this->cache->save($promocode);
        }
        return null;
    }

    /**
     * Редактирование промокода
     * @param null $parameters
     * @return CartPromoCodeModel|null
     */
    public function editPromoCode($parameters = null): ?CartPromoCodeModel
    {
        if (!is_array($parameters)) { return null; }

        $id = $parameters['id'] ?? null;
        $name = $parameters['name'] ?? null;
        $code = $parameters['code'] ?? null;
        $description = $parameters['description'] ?? null;
        $treeId = $parameters['tree_id'] ?? $parameters['treeId'] ?? null;
        $active = $parameters['active'] ?? null;

        $alone = $parameters['alone'] ?? null;
        $reusable = $parameters['reusable'] ?? null;
        $applied = $parameters['applied'] ?? null;

        $conditionId = $parameters['condition_id'] ?? $parameters['conditionId'] ?? null;
        $actionPriceId = $parameters['action_price_id'] ?? $parameters['actionPriceId'] ?? null;

        $promocode = $this->validator->isValidId($id) ? $this->repo->getPromoCodeById((int)$id) : null;
        if (!$promocode) {
            $promocode =  new CartPromoCode();
        }

        //Наименование
        if (!is_null($name) && $this->validator->isValidEmptyText($name)) {
            if (!$name) {
                $promocode->setName(null);
            } else if ($this->validator->isValidText($name) && ($promocode->getName() !== (string)$name)) {
                $promocode->setName((string)$name);
            }
        }

        //Код
        if (!is_null($code) && $this->validator->isValidEmptyText($code)) {
            if (!$code) {
                $promocode->setCode(null);
            } else if ($this->validator->isValidText($code) && ($promocode->getCode() !== (string)$code)) {
                $promocode->setCode((string)$code);
            }
        }

        //Описание
        if (!is_null($description) && $this->validator->isValidEmptyText($description)) {
            if (!$description) {
                $promocode->setDescription(null);
            } else if ($this->validator->isValidText($description) && ($promocode->getDescription() !== (string)$description)) {
                $promocode->setDescription((string)$description);
            }
        }

        //Идентификатор дерева
        if (!is_null($treeId) && $this->validator->isValidInt($treeId)) {
            if (!$treeId) {
                $promocode->setTreeId(null);
            } else if ($this->validator->isValidId($treeId) && ($promocode->getTreeId() !== (int)$treeId)) {
                $promocode->setTreeId((int)$treeId);
            }
        }

        //Активность
        if (!is_null($active) && $this->validator->isValidBool($active)) {
            $promocode->setActive($active === true);
        }

        //Одинокий )
        if (!is_null($alone) && $this->validator->isValidBool($alone)) {
            $promocode->setAlone($alone === true);
        }

        //Многоразовый
        if (!is_null($reusable) && $this->validator->isValidBool($reusable)) {
            $promocode->setReusable($reusable === true);
        }

        //Кол-во использованных промокодов
        if (!is_null($applied) && $this->validator->isValidInt($applied)) {
            if (!$applied) {
                $promocode->setApplied(0);
            } else if ($this->validator->isValidId($treeId) && ($promocode->getApplied() !== (int)$applied)) {
                $promocode->setApplied((int)$applied);
            }
        }

        //Идентификатор условия
        if (!is_null($conditionId) && $this->validator->isValidInt($conditionId)) {
            if (!$conditionId) {
                $promocode->setConditionId(null);
            } else if ($this->validator->isValidId($conditionId) && ($promocode->getConditionId() !== (int)$conditionId)) {
                $promocode->setConditionId((int)$conditionId);
            }
        }

        //Идентификатор действия для цены
        if (!is_null($actionPriceId) && $this->validator->isValidInt($actionPriceId)) {
            if (!$actionPriceId) {
                $promocode->setActionPriceId(null);
            } else if ($this->validator->isValidId($actionPriceId) && ($promocode->getActionPriceId() !== (int)$actionPriceId)) {
                $promocode->setActionPriceId((int)$actionPriceId);
            }
        }

        $this->repo->savePromoCode($promocode);
        return $this->cache->save($promocode);
    }


    /**
     * Удаление промокода
     * @param $id
     * @return bool
     */
    public function deletePromoCode($id): bool
    {
        if (!$this->validator->isValidId($id)) {
            $this->setMessage('Неверный идентификатор');
            return false;
        }

        $promocode = $this->repo->getPromoCodeById($id);
        if (!$promocode) {
            $this->setMessage('Нет промокода с данным идентификатором');
            return false;
        }

        $this->cache->delete($id);
        $this->cache->deleteCode($promocode->getCode());
        $this->repo->deletePromoCode($promocode);
        return true;
    }

    /**
     * Применить промокод
     * @param $id
     * @return CartPromoCodeModel|null
     */
    public function applyPromoCode($id) {
        $this->repo->apply($id);
        $promocode = $this->repo->getPromoCodeById($id);
        if ($promocode) {
            return $this->cache->save($promocode);
        }
        return null;
    }
}
