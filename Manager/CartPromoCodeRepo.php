<?php


namespace becompact\CartBundle\Manager;


use becompact\CartBundle\Entity\CartPromoCode;
use Doctrine\ORM\EntityManagerInterface;

class CartPromoCodeRepo
{
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @param array|null $params
     * @return CartPromoCode|null
     */
    private function findPromoCodeByParams(?array $params = null): ?CartPromoCode
    {
        return is_array($params) ?  $this->em->getRepository(CartPromoCode::class)->findOneBy($params) : null;
    }

    /**
     * @param int|null $id
     * @param null $active
     * @return CartPromoCode|null
     */
    public function getPromoCodeById(?int $id, $active = null): ?CartPromoCode
    {
        $params = [ 'id' => $id ];
        if (is_bool($active)) { $params['active'] = $active === true; }
        return $this->findPromoCodeByParams($params);
    }

    /**
     * @param string|null $code
     * @param null $active
     * @return CartPromoCode|null
     */
    public function getPromoCode(?string $code, $active = null): ?CartPromoCode
    {
        $params = [ 'code' => $code ];
        if (is_bool($active)) { $params['active'] = $active === true; }
        return $this->findPromoCodeByParams($params);
    }

    /**
     * @param $id
     * @param null $active
     * @return array
     */
    public function getPromoCodesByTreeId($id, $active = null): array
    {
        $params = ["treeId" => (int)$id ];
        if (is_bool($active)) { $params['active'] = $active === true; }
        return $this->em->getRepository(CartPromoCode::class)->findBy($params);
    }

    /**
     * @param CartPromoCode $promocode
     */
    public function savePromoCode(CartPromoCode &$promocode)
    {
        $this->em->persist($promocode);
        $this->em->flush();
    }

    /**
     * @param CartPromoCode $promocode
     */
    public function deletePromoCode(CartPromoCode $promocode)
    {
        $this->em->remove($promocode);
        $this->em->flush();
    }

    /**
     * @param int|null $id
     */
    public function apply(?int $id)
    {
        $this->em->getRepository(CartPromoCode::class)->incApplied($id);
    }

    /**
     * @param $object
     */
    public function persist($object)
    {
        $this->em->persist($object);
    }

    /**
     * @param $object
     */
    public function remove($object) {
        $this->em->remove($object);
    }
}
