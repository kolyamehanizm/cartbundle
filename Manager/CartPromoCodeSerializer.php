<?php


namespace becompact\CartBundle\Manager;


use becompact\CartBundle\Entity\CartPromoCode;
use becompact\CartBundle\Normalizers\CartPromoCodeNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;

class CartPromoCodeSerializer
{
    public function serialize(?CartPromoCode $promocode): ?string
    {
        if ($promocode instanceof CartPromoCode) {
            $encoders = [new JsonEncoder()];
            $normalizers = [
                new CartPromoCodeNormalizer(),
            ];

            $serializer = new Serializer($normalizers, $encoders);
            return $serializer->serialize($promocode, 'json');
        }
        return null;
    }
}
