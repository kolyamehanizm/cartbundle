<?php


namespace becompact\CartBundle\Model;


use becompact\Model\ModelPrototype;

class CartActionPriceModel
{
    use ModelPrototype;

    protected $id;
    protected $name;
    protected $treeId;
    protected $free;
    protected $discount;
    protected $percent;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getTreeId(): ?int
    {
        return $this->treeId;
    }

    public function getFree(): bool
    {
        return $this->free === true;
    }

    public function getPercent(): bool
    {
        return $this->percent === true;
    }

    public function getDiscount(): ?float
    {
        return is_numeric($this->discount) ? (float)$this->discount : null;
    }
}
