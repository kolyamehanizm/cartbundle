<?php

namespace becompact\CartBundle\Model;

use becompact\Model\ModelPrototype;

class CartConditionModel
{
    use ModelPrototype;

    protected array $mappingClasses = [
        'articles' => ConditionArticlesModel::class,
        'properties' => ConditionPropertiesModels::class,
        'brands' => ConditionBrandsModel::class,
        'prices' => ConditionPricesModel::class,
        'counts' => ConditionCountsModel::class,
        'deliveries' => ConditionDeliveriesModel::class,
        'categories' => ConditionCategoriesModel::class,
    ];

    protected $id;
    // protected $extId;
    protected $name;
    protected $treeId;
    protected $active;

    protected $articles;
    protected $properties;
    protected $brands;
    protected $prices;
    protected $counts;
    protected $deliveries;
    protected $categories;


    public function getId(): ?int
    {
        return $this->id;
    }

    /*
    public function getExtId(): ?int
    {
        return $this->extId;
    }
    */

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getTreeId(): ?int
    {
        return $this->treeId;
    }

    public function getActive(): bool
    {
        return $this->active === true;
    }

    public function getArticles(): ?ConditionArticlesModel
    {
        return $this->articles instanceof ConditionArticlesModel ? $this->articles : null;
    }

    public function getProperties(): ?array
    {
        return $this->properties instanceof ConditionPropertiesModels ? $this->properties->getAll() : [];
    }

    public function getBrands(): ?ConditionBrandsModel
    {
        return $this->brands instanceof ConditionBrandsModel ? $this->brands : null;
    }

    public function getPrices(): ?ConditionPricesModel
    {
        return $this->prices instanceof ConditionPricesModel ? $this->prices : null;
    }

    public function getCounts(): ?ConditionCountsModel
    {
        return $this->counts instanceof ConditionCountsModel ? $this->counts : null;
    }

    public function getDeliveries(): ?ConditionDeliveriesModel
    {
        return $this->deliveries instanceof ConditionDeliveriesModel ? $this->deliveries : null;
    }

    public function getCategories(): ?ConditionCategoriesModel
    {
        return $this->categories instanceof ConditionCategoriesModel ? $this->categories : null;
    }
}
