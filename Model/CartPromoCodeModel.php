<?php


namespace becompact\CartBundle\Model;


use becompact\Model\ModelPrototype;

class CartPromoCodeModel
{
    use ModelPrototype;

    protected array $mappingClasses = [
        'condition' => CartConditionModel::class,
        'actionPrice' => CartActionPriceModel::class,
    ];

    protected $id;
    protected $name;
    protected $code;
    protected $treeId;
    protected $active;
    protected $description;
    protected $alone;
    protected $reusable;
    protected $applied;

    protected $conditionId;
    protected $condition;

    protected $actionPriceId;
    protected $actionPrice;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function getTreeId(): ?int
    {
        return $this->treeId;
    }

    public function getActive(): bool
    {
        return $this->active === true;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getAlone(): bool
    {
        return $this->alone === true;
    }

    public function getReusable(): bool
    {
        return $this->reusable === true;
    }

    public function getApplied(): ?int
    {
        return $this->applied;
    }

    public function getConditionId(): ?int
    {
        return $this->conditionId;
    }

    public function getCondition(): ?CartConditionModel
    {
        return $this->condition instanceof CartConditionModel ? $this->condition : null;
    }

    public function setCondition($condition)
    {
        if ($condition instanceof CartConditionModel) {
            $this->condition = $condition;
        }
    }

    public function getActionPriceId(): ?int
    {
        return $this->actionPriceId;
    }

    public function getActionPrice(): ?CartActionPriceModel
    {
        return $this->actionPrice instanceof CartActionPriceModel ? $this->actionPrice : null;
    }

    public function setActionPrice($action)
    {
        if ($action instanceof CartActionPriceModel) {
            $this->actionPrice = $action;
        }
    }
}
