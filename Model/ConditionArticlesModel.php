<?php

namespace becompact\CartBundle\Model;

use becompact\Model\ModelPrototype;

class ConditionArticlesModel
{
    use ModelPrototype;

    protected $include;
    protected $exclude;

    public function getInclude(): ?array
    {
        return is_array($this->include) ? $this->include : null;
    }

    public function getExclude(): ?array
    {
        return is_array($this->exclude) ? $this->exclude : null;
    }

}
