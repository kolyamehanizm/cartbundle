<?php

namespace becompact\CartBundle\Model;

use becompact\Model\ModelPrototype;

class ConditionCountsModel
{
    use ModelPrototype;

    protected $min;
    protected $multiple;

    public function getMin(): ?int
    {
        return is_numeric($this->min) ? (int)$this->min : null;
    }

    public function isMultiple(): ?bool
    {
        return is_bool($this->multiple) ? $this->multiple === true : null;
    }

}
