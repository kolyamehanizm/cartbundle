<?php

namespace becompact\CartBundle\Model;

use becompact\Model\ModelPrototype;

class ConditionDeliveriesModel
{
    use ModelPrototype;

    protected $deliveries;

    public function getDeliveries(): ?array
    {
        return is_array($this->deliveries) ? $this->deliveries : null;
    }


}
