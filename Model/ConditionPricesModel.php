<?php

namespace becompact\CartBundle\Model;

use becompact\Model\ModelPrototype;

class ConditionPricesModel
{
    use ModelPrototype;

    protected $min;
    protected $max;

    public function getMin(): ?float
    {
        return is_numeric($this->min) ? (float)$this->min : null;
    }

    public function getMax(): ?float
    {
        return is_numeric($this->max) ? (float)$this->max : null;
    }

}
