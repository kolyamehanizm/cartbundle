<?php

namespace becompact\CartBundle\Model;

use becompact\Model\ModelPrototype;

class ConditionPropertiesModel
{
    use ModelPrototype;

    protected $propertyId;
    protected $propertyCode;
    protected $term;
    protected $propertyValues;

    public function getPropertyId(): ?int
    {
        return is_numeric($this->propertyId) ? (int)$this->propertyId : null;
    }

    public function getPropertyCode(): ?string
    {
        return is_string($this->propertyCode) ? $this->propertyCode : null;
    }


    public function getTerm(): ?string
    {
        return is_string($this->term) ? $this->term : null;
    }

    public function getPropertyValues(): ?array
    {
        return is_array($this->propertyValues) ? $this->propertyValues : null;
    }


}
