<?php

namespace becompact\CartBundle\Model;

use becompact\Model\ObjectPrototype;

class ConditionPropertiesModels extends ObjectPrototype
{
    /**
     * @param array|object|null $data
     *
     * @return ConditionPropertiesModels
     */
    public function add($data)
    {
        if (is_array($data)) {
            $this->collection[] = new ConditionPropertiesModel($data);
        } elseif ($data instanceof ConditionPropertiesModel) {
            $this->collection[] = $data;
        }
        return $this;
    }

    /**
     * Get items
     *
     * @return ConditionPropertiesModel[]
     */
    public function getAll(): array
    {
        return $this->collection;
    }

    /**
     * @return ConditionPropertiesModel
     */
    public function current(): ConditionPropertiesModel
    {
        return parent::current();
    }
}
