<?php


namespace becompact\CartBundle\Normalizers;


use becompact\CartBundle\Entity\CartActionPrice;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerAwareTrait;

class CartActionPriceNormalizer implements NormalizerInterface, SerializerAwareInterface
{
    use SerializerAwareTrait;


    public function normalize($object, $format = null, array $context = [])
    {
        return [
            'id' => $object->getId(),
            'name' =>  $object->getName(),
            'treeId' => $object->getTreeId(),
            'free' => $object->getFree() === true,
            'discount' => $object->getDiscount(),
            'percent' => $object->getPercent() === true,
        ];
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof CartActionPrice;
    }
}
