<?php

namespace becompact\CartBundle\Normalizers;

use becompact\CartBundle\Entity\CartCondition;
use becompact\CartBundle\Entity\ConditionArticles;


use becompact\CartBundle\Entity\ConditionBrands;
use becompact\CartBundle\Entity\ConditionCategories;
use becompact\CartBundle\Entity\ConditionCounts;
use becompact\CartBundle\Entity\ConditionDeliveries;
use becompact\CartBundle\Entity\ConditionPrices;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerAwareTrait;

class CartConditionNormalizer implements NormalizerInterface, SerializerAwareInterface
{
    use SerializerAwareTrait;

    public function normalize($object, $format = null, array $context = [])
    {
        $result = [
            'id' => $object->getId(),
            // 'extId' => $object->getExtId(),
            'name' =>  $object->getName(),
            'treeId' => $object->getTreeId(),
            'active' => $object->getActive() === true,
        ];

        $articles = $object->getArticles();
        if ($articles instanceof ConditionArticles) {
            $result['articles'] = $this->serializer->normalize($articles, $format, $context);
        }

        $properties = $object->getProperties();
        if ($properties && count($properties) > 0) {
            $result['properties'] = array_map(
                function ($object) use ($format, $context) {
                    return $this->serializer->normalize($object, $format, $context);
                },
                $properties->toArray()
            );
        }

        $brands = $object->getBrands();
        if ($brands instanceof ConditionBrands) {
            $result['brands'] = $this->serializer->normalize($brands, $format, $context);
        }

        $prices = $object->getPrices();
        if ($prices instanceof ConditionPrices) {
            $result['prices'] = $this->serializer->normalize($prices, $format, $context);
        }

        $counts = $object->getCounts();
        if ($prices instanceof ConditionCounts) {
            $result['counts'] = $this->serializer->normalize($counts, $format, $context);
        }

        $deliveries = $object->getDeliveries();
        if ($deliveries instanceof ConditionDeliveries) {
            $result['deliveries'] = $this->serializer->normalize($deliveries, $format, $context);
        }

        $categories = $object->getCategories();
        if ($categories instanceof ConditionCategories) {
            $result['categories'] = $this->serializer->normalize($categories, $format, $context);
        }

        return $result;
    }

    /**
     * @param mixed $data
     * @param null $format
     * @return bool
     */
    public function supportsNormalization($data, $format = null): bool
    {
        return $data instanceof CartCondition;
    }
}
