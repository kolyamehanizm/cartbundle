<?php


namespace becompact\CartBundle\Normalizers;


use becompact\CartBundle\Entity\CartPromoCode;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerAwareTrait;

class CartPromoCodeNormalizer implements NormalizerInterface, SerializerAwareInterface
{
    use SerializerAwareTrait;


    public function normalize($object, $format = null, array $context = [])
    {
        return [
            'id' => $object->getId(),
            'name' =>  $object->getName(),
            'code' =>  $object->getCode(),
            'treeId' => $object->getTreeId(),
            'active' => $object->getActive() === true,
            'description' => $object->getDescription(),
            'alone' => $object->getAlone() === true,
            'reusable' => $object->getReusable() === true,
            'applied' => is_numeric($object->getApplied()) ? (int)$object->getApplied() : 0,
            'conditionId' => $object->getConditionId(),
            'actionPriceId' => $object->getActionPriceId(),
        ];
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof CartPromoCode;
    }
}
