<?php


namespace becompact\CartBundle\Normalizers;


use becompact\CartBundle\Entity\ConditionBrands;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerAwareTrait;

class ConditionBrandsNormalizer implements NormalizerInterface, SerializerAwareInterface
{
    use SerializerAwareTrait;

    public function normalize($object, $format = null, array $context = [])
    {
        return [
            'include' => is_array($object->getInclude()) ? $object->getInclude() : [],
            'exclude' => is_array($object->getExclude()) ? $object->getExclude() : [],
        ];
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof ConditionBrands;
    }
}
