<?php


namespace becompact\CartBundle\Normalizers;


use becompact\CartBundle\Entity\ConditionCounts;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerAwareTrait;

class ConditionCountsNormalizer implements NormalizerInterface, SerializerAwareInterface
{
    use SerializerAwareTrait;

    public function normalize($object, $format = null, array $context = [])
    {
        return [
            'min' => is_numeric($object->getMin()) ? $object->getMin() : 0,
            'multiple' => $object->getMultiple() === true,
        ];
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof ConditionCounts;
    }
}
