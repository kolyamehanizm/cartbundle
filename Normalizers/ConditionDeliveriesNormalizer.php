<?php


namespace becompact\CartBundle\Normalizers;


use becompact\CartBundle\Entity\ConditionDeliveries;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerAwareTrait;

class ConditionDeliveriesNormalizer implements NormalizerInterface, SerializerAwareInterface
{
    use SerializerAwareTrait;

    public function normalize($object, $format = null, array $context = [])
    {
        return [
            'deliveries' => is_array($object->getDeliveries()) ? $object->getDeliveries() : [],
        ];
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof ConditionDeliveries;
    }
}
