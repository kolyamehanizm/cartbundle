<?php


namespace becompact\CartBundle\Normalizers;


use becompact\CartBundle\Entity\ConditionPrices;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerAwareTrait;

class ConditionPricesNormalizer implements NormalizerInterface, SerializerAwareInterface
{
    use SerializerAwareTrait;

    public function normalize($object, $format = null, array $context = [])
    {
        return [
            'min' => is_numeric($object->getMin()) ? $object->getMin() : 0,
            'max' => is_numeric($object->getMax()) ? $object->getMax() : 0,
        ];
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof ConditionPrices;
    }
}
