<?php


namespace becompact\CartBundle\Normalizers;


use becompact\CartBundle\Entity\ConditionProperties;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerAwareTrait;

class ConditionPropertiesNormalizer implements NormalizerInterface, SerializerAwareInterface
{
    use SerializerAwareTrait;

    public function normalize($object, $format = null, array $context = [])
    {
        return [
            'propertyId' => $object->getPropertyId(),
            'propertyCode' => $object->getPropertyCode(),
            'term' => $object->getTerm(),
            'propertyValues' => is_array($object->getPropertyValues()) ? $object->getPropertyValues() : [],
        ];
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof ConditionProperties;
    }
}

