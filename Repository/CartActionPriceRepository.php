<?php


namespace becompact\CartBundle\Repository;


use becompact\CartBundle\Entity\CartActionPrice;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class CartActionPriceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CartActionPrice::class);
    }

}
