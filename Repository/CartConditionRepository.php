<?php

namespace becompact\CartBundle\Repository;

use becompact\CartBundle\Entity\CartCondition;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CartCondition|null find($id, $lockMode = null, $lockVersion = null)
 * @method CartCondition|null findOneBy(array $criteria, array $orderBy = null)
 * @method CartCondition[]    findAll()
 * @method CartCondition[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CartConditionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CartCondition::class);
    }

    // /**
    //  * @return CartСondition[] Returns an array of CartСondition objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CartСondition
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
