<?php


namespace becompact\CartBundle\Repository;


use becompact\CartBundle\Entity\CartPromoCode;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class CartPromoCodeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CartPromoCode::class);
    }

    /**
     * Увеличить счётчик использования промокода
     * @param int|null $id
     */
    public function incApplied(?int $id) {
        if (!is_numeric($id) || !$id) { return; }
        $this
            ->createQueryBuilder('p')
            ->update()
            ->set('p.applied', 'p.applied + 1')
            ->where('p.id = :id')
            ->setParameter('id', (int)$id)
            ->getQuery()
            ->execute();
    }
}
