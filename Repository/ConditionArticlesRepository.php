<?php

namespace becompact\CartBundle\Repository;

use becompact\CartBundle\Entity\ConditionArticles;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ConditionArticles|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConditionArticles|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConditionArticles[]    findAll()
 * @method ConditionArticles[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConditionArticlesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConditionArticles::class);
    }

    // /**
    //  * @return ConditionArticles[] Returns an array of ConditionArticles objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ConditionArticles
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
