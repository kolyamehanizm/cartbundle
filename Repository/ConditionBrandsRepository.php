<?php

namespace becompact\CartBundle\Repository;

use becompact\CartBundle\Entity\ConditionBrands;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ConditionBrands|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConditionBrands|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConditionBrands[]    findAll()
 * @method ConditionBrands[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConditionBrandsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConditionBrands::class);
    }

    // /**
    //  * @return ConditionBrands[] Returns an array of ConditionBrands objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ConditionBrands
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
