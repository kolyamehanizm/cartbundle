<?php

namespace becompact\CartBundle\Repository;

use becompact\CartBundle\Entity\ConditionCategories;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ConditionCategories|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConditionCategories|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConditionCategories[]    findAll()
 * @method ConditionCategories[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConditionCategoriesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConditionCategories::class);
    }

    // /**
    //  * @return ConditionCategories[] Returns an array of ConditionCategories objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ConditionCategories
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
