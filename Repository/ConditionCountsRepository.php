<?php

namespace becompact\CartBundle\Repository;

use becompact\CartBundle\Entity\ConditionCounts;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ConditionCounts|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConditionCounts|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConditionCounts[]    findAll()
 * @method ConditionCounts[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConditionCountsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConditionCounts::class);
    }

    // /**
    //  * @return ConditionCounts[] Returns an array of ConditionCounts objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ConditionCounts
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
