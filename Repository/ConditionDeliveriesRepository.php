<?php

namespace becompact\CartBundle\Repository;

use becompact\CartBundle\Entity\ConditionDeliveries;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ConditionDeliveries|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConditionDeliveries|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConditionDeliveries[]    findAll()
 * @method ConditionDeliveries[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConditionDeliveriesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConditionDeliveries::class);
    }

    // /**
    //  * @return ConditionDeliveries[] Returns an array of ConditionDeliveries objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ConditionDeliveries
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
