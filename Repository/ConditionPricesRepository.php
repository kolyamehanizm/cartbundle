<?php

namespace becompact\CartBundle\Repository;

use becompact\CartBundle\Entity\ConditionPrices;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ConditionPrices|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConditionPrices|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConditionPrices[]    findAll()
 * @method ConditionPrices[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConditionPricesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConditionPrices::class);
    }

    // /**
    //  * @return ConditionPrices[] Returns an array of ConditionPrices objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ConditionPrices
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
