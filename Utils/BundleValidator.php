<?php

namespace becompact\CartBundle\Utils;

use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;

class BundleValidator
{
    private ValidatorInterface $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function isValidId($id): bool
    {
        $constraint = [
            new Assert\Type('integer'),
            new Assert\Positive(),
        ];

        $errors = $this->validator->validate(
            $id,
            $constraint
        );

        return 0 === count($errors);
    }

    public function isValidInt($id): bool
    {
        $constraint = [
            new Assert\Type('integer'),
            new Assert\PositiveOrZero(),
        ];

        $errors = $this->validator->validate(
            $id,
            $constraint
        );

        return 0 === count($errors);
    }

    public function isValidFloat($id): bool
    {
        $constraint = [
            new Assert\Type('float'),
            new Assert\PositiveOrZero(),
        ];

        $errors = $this->validator->validate(
            $id,
            $constraint
        );

        return 0 === count($errors);
    }


    public function isValidBool($bool): bool
    {
        $constraint = [
            new Assert\Type('bool'),
        ];

        $errors = $this->validator->validate(
            $bool,
            $constraint
        );

        return 0 === count($errors);
    }

    public function isValidArray($arr): bool
    {
        $constraint = [
            new Assert\Type('array'),
        ];

        $errors = $this->validator->validate(
            $arr,
            $constraint
        );

        return 0 === count($errors);
    }

    public function isValidArrayValues($arr): bool
    {
        $constraint = [
            new Assert\Type('array'),
            new Assert\Count(['min' => 1]),
        ];

        $errors = $this->validator->validate(
            $arr,
            $constraint
        );

        return 0 === count($errors);
    }

    public function isValidText($text): bool
    {
        $constraint = [
            new Assert\Type('string'),
            new Assert\NotBlank(),
        ];

        $errors = $this->validator->validate(
            $text,
            $constraint
        );

        return 0 === count($errors);
    }

    public function isValidEmptyText($text): bool
    {
        $constraint = [
            new Assert\Type('string'),
        ];

        $errors = $this->validator->validate(
            $text,
            $constraint
        );

        return 0 === count($errors);
    }
}
